sortirdefacebook
================

Brochure sur les enjeux et les alternatives d'une sortie de facebook.

<https://sortirdefacebook.wordpress.com>

Ce dépôt propose des versions:

- org-mode (format d'origine, pour le logiciel Emacs, qui permet de transformer en…)
- latex
- pdf
- html
- odt (pour LibreOffice)
- txt

**Nous recherchons une bonne âme pour réaliser une belle couverture !** (de préférence en latex). Merci d'avance !

Pour reprendre la brochure à votre compte, vous avez différents
formats à disposition : texte, html, latex et org. Le .org est un
fichier texte qui a une signification particulière pour l'éditeur
de texte Gnu Emacs. Voici comment le modifier.


Installez emacs et une version récente d'org-mode. Sous Debian Gnu/Linux :

    apt-get install emacs org-mode

Ouvrez le .org avec emacs et faites vos modifications. Voyez le menu
qui vous donne des options d'édition et vous indique les raccourcis
claviers.

Ensuite on a besoin d'exporter ce fichier .org dans un autre format,
pdf par exemple. Vous pouvez passer par le menu : Org ->
Export/Publish… lisez les choix possibles et tapez 'p' pour pdf.


C'est rapide et propre, mais il est des fois nécessaire de passer par
latex pour avoir la pleine maitrise de ce qu'on veut faire (on
peut insérer directement du code latex dans un fichier org-mode).

Plus d'infos : <http://orgmode.org/>


ps: il devrait être possible, dans la dernière version d'orgmode,
d'exporter le contenu dans un format odt (pour
LibreOffice). Faites-moi signe si cela vous intéresse.

### Dépendances latex

Paquets apt pour latex:

    texlive-latex-extra
    texlive-generic-recommended
    tex-common
    latex-xcolor
